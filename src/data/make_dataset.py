from typing import Dict

from datasets import Dataset, load_dataset
from transformers import AutoTokenizer


def load_sst2_dataset(tokenizer: AutoTokenizer) -> Dict[str, Dataset]:
    sst2_dataset = load_dataset("glue", "sst2")
    return {
        key: _transform_sst2(dataset, tokenizer)
        for key, dataset in sst2_dataset.items()
    }


def _transform_sst2(dataset: Dataset, tokenizer: AutoTokenizer) -> Dataset:
    dataset = dataset.rename_column("sentence", "text")
    return _tokenize_dataset(dataset, tokenizer)


def _tokenize_dataset(dataset: Dataset, tokenizer: AutoTokenizer) -> Dataset:
    return (
        dataset.map(tokenizer, input_columns="text")
        .add_column("label", dataset["label"])
        .remove_columns("text")
    )
