# pylint: disable=invalid-name
import hashlib
from enum import Enum
from typing import Any, Dict

import torch
import typer
from transformers import (
    AutoConfig,
    AutoTokenizer,
    GPT2ForSequenceClassification,
    Trainer,
    TrainingArguments,
)

import wandb
from src.data.make_dataset import load_sst2_dataset
from src.models.metrics import compute_sst2_metrics
from src.models.model import (
    PrefixPromptTrainingGPT2ForSequenceClassification,
    SuffixPromptTrainingGPT2ForSequenceClassification,
)
from src.paths import MODEL_RUN_FOLDER, MODELS_FOLDER


class ModelType(str, Enum):
    finetune = "fine-tune"
    suffix = "suffix-prompt"
    prefix = "prefix-prompt"


def main(  # pylint: disable=too-many-locals
    *,
    model_type: ModelType = typer.Option(...),
    prompt_tokens: int = 5,
    epochs: int = 5,
    model_name: str = "gpt2",
    batch_size: int = 128,
    evaluation_round: int = 0,
):
    config = {
        "model-type": model_type.value,
        "model": model_name,
        "prompt-tokens": prompt_tokens,
        "epochs": epochs,
        "batch-size": batch_size,
        "round": evaluation_round,
    }
    name = get_run_name(config)
    run = wandb.init(
        project="prompt-training",
        name=name,
        config=config,
    )

    print(wandb.config)
    print(wandb.run.name)

    prompt_tokens = run.config["prompt-tokens"]
    epochs = run.config["epochs"]
    model_name = run.config["model"]
    batch_size = run.config["batch-size"]

    model_config = AutoConfig.from_pretrained(model_name)
    model_config.pad_token_id = model_config.eos_token_id
    model_config.prompt_tokens = prompt_tokens
    model_config.num_labels = 2

    if model_type == ModelType.suffix:
        model = SuffixPromptTrainingGPT2ForSequenceClassification.from_pretrained(
            model_name, config=model_config
        )
        optimizer = torch.optim.AdamW(
            list(model.prompt.parameters()) + list(model.score.parameters())
        )
    elif model_type == ModelType.prefix:
        model = PrefixPromptTrainingGPT2ForSequenceClassification.from_pretrained(
            model_name, config=model_config
        )
        optimizer = torch.optim.AdamW(
            list(model.prompt.parameters()) + list(model.score.parameters())
        )
    elif model_type == ModelType.finetune:
        model = GPT2ForSequenceClassification.from_pretrained(
            model_name, config=model_config
        )
        optimizer = None  # let transformers make it
    else:
        raise NotImplementedError(f"model-type {model_type.value} not supported yet")

    model.train()

    tokenizer = AutoTokenizer.from_pretrained(model_name, pad_token="<|endoftext|>")
    dataset = load_sst2_dataset(tokenizer)

    training_args = TrainingArguments(
        report_to=["wandb"],
        warmup_ratio=0.06,
        output_dir=MODEL_RUN_FOLDER / "output",
        overwrite_output_dir=True,
        per_device_train_batch_size=batch_size,
        per_device_eval_batch_size=batch_size,
        learning_rate=5e-5,
        num_train_epochs=epochs,
        evaluation_strategy="epoch",
        logging_dir=MODEL_RUN_FOLDER / "output",
        logging_steps=100,
        load_best_model_at_end=True,
        metric_for_best_model="accuracy",
        greater_is_better=True,
    )

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=dataset["train"],
        eval_dataset=dataset["validation"],
        tokenizer=tokenizer,
        compute_metrics=compute_sst2_metrics,
        optimizers=(optimizer, None),
    )

    trainer.train()
    run.finish()

    model_folder = MODELS_FOLDER / name
    model_folder.mkdir(exist_ok=True, parents=True)
    model.save_pretrained(model_folder)


def get_run_name(config_dict: Dict[str, Any]) -> str:
    parameters = [
        (config_dict["model"], "model"),
        (config_dict["model-type"], "model-type"),
        (config_dict["epochs"], "epochs"),
        (config_dict["batch-size"], "batch-size"),
    ]
    if config_dict["model-type"] != "fine-tune":
        parameters += [(config_dict["prompt-tokens"], "prompt-tokens")]

    return "_".join(
        [f"{value}_{label}" for value, label in parameters]
        + [get_id_for_dict(config_dict)]
    )


def get_id_for_dict(config_dict: Dict[str, Any]) -> str:
    """This function creates a unique hash
    based on the initial config dictionary
    that is used to label the model."""

    unique_str = "".join(
        f"'{key}':'{value}';" for key, value in sorted(config_dict.items())
    )
    return hashlib.sha1(unique_str.encode("utf-8")).hexdigest()[:5]


if __name__ == "__main__":
    typer.run(main)
