"""
This defines the prompt training models.
"""

# pylint: disable=abstract-method, arguments-differ, too-many-ancestors, invalid-name

import logging
from typing import TypeVar

import transformers.models.gpt2.modeling_gpt2 as gpt2_module
from transformers import GPT2ForSequenceClassification

from src.models.modules.prompt import PrefixPrompt, SuffixPrompt

# disable warning about padding tokens:
# > MODEL will not detect padding tokens in `inputs_embeds`. Results may be
# > unexpected if using padding tokens in conjunction with `inputs_embeds.`
gpt2_module.logger.setLevel(logging.CRITICAL)

# Copied from torch/nn/modules/module.py:
# See https://mypy.readthedocs.io/en/latest/generics.html#generic-methods-and-generic-self
# for the use of `T` to annotate `self`. Many methods of `Module` return `self`
# and we want those return values to be the type of the subclass, not the
# looser type of `Module`.
T = TypeVar("T", bound="Module")


class PrefixPromptTrainingGPT2ForSequenceClassification(GPT2ForSequenceClassification):
    """
    Adds the prompt before the sequence tokens and then does inference.
    """

    def __init__(self: T, config) -> None:
        super().__init__(config)

        assert self.config.pad_token_id is not None
        self.prompt = PrefixPrompt(
            prompt_tokens=config.prompt_tokens,
            embedding=self.transformer.wte,
        )

    def train(self: T, mode: bool = True) -> T:
        self.transformer = self.transformer.train(False)  # put core in eval mode
        self.prompt = self.prompt.train(mode)
        self.score = self.score.train(mode)
        return self

    def forward(
        self: T,
        input_ids=None,
        attention_mask=None,
        **kwargs,
    ):
        """
        This converts the input_ids into the embedding space and adds the prompt onto it.
        Then the base forward method can be invoked.
        """
        inputs_embeds, attention_mask = self.prompt(
            input_ids=input_ids,
            attention_mask=attention_mask,
            embedding=self.transformer.wte,
        )
        return super().forward(
            inputs_embeds=inputs_embeds, attention_mask=attention_mask, **kwargs
        )


class SuffixPromptTrainingGPT2ForSequenceClassification(GPT2ForSequenceClassification):
    """
    Adds the prompt after the sequence tokens and then does inference.
    """

    def __init__(self, config) -> None:
        super().__init__(config)

        assert self.config.pad_token_id is not None
        self.prompt = SuffixPrompt(
            prompt_tokens=config.prompt_tokens,
            pad_token_id=config.pad_token_id,
            embedding=self.transformer.wte,
        )

    def train(self: T, mode: bool = True) -> T:
        self.transformer = self.transformer.train(False)  # put core in eval mode
        self.score = self.score.train(mode)
        return self

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        **kwargs,
    ):
        """
        This converts the input_ids into the embedding space and adds the prompt onto it.
        Then the base forward method can be invoked.
        """
        inputs_embeds, attention_mask = self.prompt(
            input_ids=input_ids,
            attention_mask=attention_mask,
            embedding=self.transformer.wte,
        )
        return super().forward(
            inputs_embeds=inputs_embeds, attention_mask=attention_mask, **kwargs
        )
