from typing import Dict

from datasets import load_metric
from transformers import EvalPrediction

sst2_metric = load_metric("glue", "sst2")


def compute_sst2_metrics(run: EvalPrediction) -> Dict[str, float]:
    targets = run.label_ids
    predictions = run.predictions.argmax(axis=1)
    return sst2_metric.compute(predictions=predictions, references=targets)
