"""
This defines the prompt module.
"""

from abc import abstractmethod
from typing import Optional, Tuple

import torch
import torch.nn as nn


class Prompt(nn.Module):
    def __init__(self, prompt_tokens: int, embedding: nn.Embedding) -> None:
        super().__init__()
        vocab_size = embedding.weight.shape[0]
        prompt_indexes = torch.randint(
            size=(prompt_tokens,),
            low=0,
            high=vocab_size,
            device=embedding.weight.device,
        )
        self.prompt = torch.nn.Parameter(embedding(prompt_indexes).clone()[None, :, :])

    @abstractmethod
    def forward(
        self,
        input_ids: torch.Tensor,
        attention_mask: Optional[torch.Tensor],
        embedding: nn.Embedding,
    ) -> Tuple[torch.Tensor, Optional[torch.Tensor]]:
        pass


class PrefixPrompt(Prompt):
    def forward(
        self,
        input_ids: torch.Tensor,
        attention_mask: Optional[torch.Tensor],
        embedding: nn.Embedding,
    ) -> Tuple[torch.Tensor, Optional[torch.Tensor]]:
        inputs_embeds = embedding(input_ids)
        inputs_embeds = self._extend_inputs_embeds(inputs_embeds)

        if attention_mask is not None:
            attention_mask = self._extend_attention_mask(attention_mask)
        return inputs_embeds, attention_mask

    def _extend_inputs_embeds(self, inputs_embeds: torch.Tensor) -> torch.Tensor:
        batch_size = inputs_embeds.shape[0]
        prompt = self.prompt.repeat_interleave(batch_size, dim=0)
        return torch.cat(
            [
                prompt,
                inputs_embeds,
            ],
            dim=1,
        )

    def _extend_attention_mask(self, attention_mask: torch.Tensor) -> torch.Tensor:
        batch_size = attention_mask.shape[0]
        prompt_size = self.prompt.shape[1]
        return torch.cat(
            [
                torch.ones((batch_size, prompt_size), device=attention_mask.device),
                attention_mask,
            ],
            dim=1,
        )


class SuffixPrompt(Prompt):
    def __init__(
        self,
        prompt_tokens: int,
        pad_token_id: int,
        embedding: nn.Embedding,
    ) -> None:
        super().__init__(prompt_tokens=prompt_tokens, embedding=embedding)

        # We add this to the input before adding the prompt tokens, it's full of the padding tokens.
        # Making it a parameter means it will get put on the right device and
        # loss will work even though it is not trained.
        input_extension = torch.ones(
            (prompt_tokens,), dtype=int, device=embedding.weight.device
        )
        input_extension *= pad_token_id
        self.input_extension = torch.nn.Parameter(
            embedding(input_extension)[None, :, :]
        )

    def forward(
        self,
        input_ids: torch.Tensor,
        attention_mask: Optional[torch.Tensor],
        embedding: nn.Embedding,
    ) -> Tuple[torch.Tensor, Optional[torch.Tensor]]:
        inputs_embeds = embedding(input_ids)

        if attention_mask is None:
            inputs_embeds = self._add_prompt(inputs_embeds)
            return inputs_embeds, None

        inputs_embeds = self._extend_inputs_embeds(inputs_embeds)
        attention_mask = self._extend_attention_mask(attention_mask)
        self._copy_prompt(
            inputs_embeds=inputs_embeds,
            attention_mask=attention_mask,
        )

        return inputs_embeds, attention_mask

    def _add_prompt(self, inputs_embeds: torch.Tensor) -> torch.Tensor:
        batch_size = inputs_embeds.shape[0]
        prompt = self.prompt.repeat_interleave(batch_size, dim=0)
        return torch.cat(
            [
                inputs_embeds,
                prompt,
            ],
            dim=1,
        )

    def _extend_inputs_embeds(self, inputs_embeds: torch.Tensor) -> torch.Tensor:
        batch_size = inputs_embeds.shape[0]
        input_extension = self.input_extension.repeat_interleave(batch_size, dim=0)
        return torch.cat(
            [
                inputs_embeds,
                input_extension,
            ],
            dim=1,
        )

    def _extend_attention_mask(self, attention_mask: torch.Tensor) -> torch.Tensor:
        batch_size = attention_mask.shape[0]
        prompt_size = self.prompt.shape[1]
        return torch.cat(
            [
                attention_mask,
                # This is zeros because the populated indexes will be set to 1 in _copy_prompt
                torch.zeros((batch_size, prompt_size), device=attention_mask.device),
            ],
            dim=1,
        )

    def _copy_prompt(
        self, inputs_embeds: torch.Tensor, attention_mask: torch.Tensor
    ) -> None:
        prompt = self.prompt
        prompt_size = prompt.shape[1]
        attention_indexes = attention_mask.sum(dim=1).long().tolist()
        for batch_index, token_index in enumerate(attention_indexes):
            end_index = token_index + prompt_size
            inputs_embeds[batch_index, token_index:end_index] = prompt[0]
            attention_mask[batch_index, token_index:end_index] = 1
