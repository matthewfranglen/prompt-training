from pathlib import Path

PROJECT_ROOT = Path(__file__).resolve().parents[1]
DATA_FOLDER = PROJECT_ROOT / "data"
MODELS_FOLDER = PROJECT_ROOT / "models"
MODEL_RUN_FOLDER = MODELS_FOLDER / "runs"
