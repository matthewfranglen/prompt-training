"""
Check that the prefix and suffix prompt models actually work.
"""

import pytest
from transformers import AutoConfig, AutoTokenizer

from src.models.model import (
    PrefixPromptTrainingGPT2ForSequenceClassification,
    SuffixPromptTrainingGPT2ForSequenceClassification,
)


@pytest.mark.parametrize("model_name", ["gpt2"])
@pytest.mark.parametrize("prompt_tokens", [5])
def test_prefix_prompt_with_attention_mask(model_name: str, prompt_tokens: int) -> None:
    model_config = AutoConfig.from_pretrained(model_name)
    model_config.pad_token_id = model_config.eos_token_id
    model_config.prompt_tokens = prompt_tokens
    model_config.num_labels = 2
    model = PrefixPromptTrainingGPT2ForSequenceClassification.from_pretrained(
        model_name, config=model_config
    )
    tokenizer = AutoTokenizer.from_pretrained(model_name, pad_token="<|endoftext|>")

    inputs = tokenizer("hello world", return_tensors="pt")
    model(**inputs)


@pytest.mark.parametrize("model_name", ["gpt2"])
@pytest.mark.parametrize("prompt_tokens", [5])
def test_prefix_prompt_without_attention_mask(
    model_name: str, prompt_tokens: int
) -> None:
    model_config = AutoConfig.from_pretrained(model_name)
    model_config.pad_token_id = model_config.eos_token_id
    model_config.prompt_tokens = prompt_tokens
    model_config.num_labels = 2
    model = PrefixPromptTrainingGPT2ForSequenceClassification.from_pretrained(
        model_name, config=model_config
    )
    tokenizer = AutoTokenizer.from_pretrained(model_name, pad_token="<|endoftext|>")

    inputs = tokenizer(
        "hello world",
        return_tensors="pt",
        return_attention_mask=False,
    )
    model(**inputs)


@pytest.mark.parametrize("model_name", ["gpt2"])
@pytest.mark.parametrize("prompt_tokens", [5])
def test_suffix_prompt_with_attention_mask(model_name: str, prompt_tokens: int) -> None:
    model_config = AutoConfig.from_pretrained(model_name)
    model_config.pad_token_id = model_config.eos_token_id
    model_config.prompt_tokens = prompt_tokens
    model_config.num_labels = 2
    model = SuffixPromptTrainingGPT2ForSequenceClassification.from_pretrained(
        model_name, config=model_config
    )
    tokenizer = AutoTokenizer.from_pretrained(model_name, pad_token="<|endoftext|>")

    inputs = tokenizer("hello world", return_tensors="pt")
    model(**inputs)


@pytest.mark.parametrize("model_name", ["gpt2"])
@pytest.mark.parametrize("prompt_tokens", [5])
def test_suffix_prompt_without_attention_mask(
    model_name: str, prompt_tokens: int
) -> None:
    model_config = AutoConfig.from_pretrained(model_name)
    model_config.pad_token_id = model_config.eos_token_id
    model_config.prompt_tokens = prompt_tokens
    model_config.num_labels = 2
    model = SuffixPromptTrainingGPT2ForSequenceClassification.from_pretrained(
        model_name, config=model_config
    )
    tokenizer = AutoTokenizer.from_pretrained(model_name, pad_token="<|endoftext|>")

    inputs = tokenizer(
        "hello world",
        return_tensors="pt",
        return_attention_mask=False,
    )
    model(**inputs)
